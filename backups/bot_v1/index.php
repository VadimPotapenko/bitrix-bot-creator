<?php
#========================================== settings ==================================================#
include_once (__DIR__.'/src/crest.php'); // crest
include_once (__DIR__.'/setting.php');  // настройки бота
$logs = true;                          // false - отмена логирования
#==========================================================================================================#
if ($_REQUEST['event'] && file_exists(__DIR__.'/setting.php')) {
	writeToLog($_REQUEST, $logs, 'Новое событие');

	switch ($_REQUEST['event']) {
		case 'ONAPPINSTALL':
			### Установка и регистрация бота ###
			$install = CRest::installApp();
			$botRegister = CRest::call('imbot.register', $botParams);
			writeToLog($botRegister, $logs, 'Установка бот прошла успешно');

			### Регистрация команд ###
			foreach ($botCommands as $command) {
				$command = array_merge($command, array('BOT_ID' => $botRegister['result'], 'EVENT_COMMAND_ADD' => $handlerUrl));
				$dataCommand = array(
					'method' => 'imbot.command.register',
					'params' => $command
				);
			}

			### Подписка на события ###
			if (is_array($events) && (count($events) > 0)) {
				foreach ($events as $event) {
					$dataEvent = array(
						'method' => 'event.bind',
						'params' => array(
							'EVENT'   => $event,
							'HANDLER' => $handlerUrl
						)
					);
				}
			}

			### Процесс регистрации и подписки ###
			$botSetting = array_merge($dataCommand, $dataEvent);
			$settingRegister = CRest::callBatch($botSetting);
			writeToLog($newEvent, $logs, 'Подписка на новые события');

			### Приветствие вновь зарегистрированного бота ###
			if(strlen($firstMessage)) {
				$newMessage = CRest::call('imbot.message.add', array(
					'BOT_ID'    => $botRegister['result'],
					'DIALOG_ID' => $_REQUEST['auth']['user_id'],
					'MESSAGE'   => $firstMessage
				));
				writeToLog($newMessage, $logs, 'Приветствие вновь родившегося бота');
			}

			break;


		case 'ONIMBOTJOINCHAT':
			### Бот вступил в чат ###
			$newMessage = CRest::call('imbot.message.add', array(
				'BOT_ID'    => $_REQUEST['data']['PARAMS']['BOT_ID'],
				'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
				'MESSAGE'   => 'Привет мир!'
			));
			writeToLog($newMessage, $logs, 'Бот вступил в диалог');

			break;


		case 'ONIMBOTMESSAGEADD':
			### Сообщение боту ###
			$answer = CRest::call('imbot.message.add', array(
				'BOT_ID'    => $_REQUEST['data']['PARAMS']['BOT_ID'],
				'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
				'MESSAGE'   => 'Привет мир!'
			));
			writeToLog($answer, $logs, 'Ответ бота на сообщение: '.$_REQUEST['data']['PARAMS']['MESSAGE']);

			break;


		case 'ONIMCOMMANDADD':
			### Команда боту ###
			foreach ($_REQUEST['data']['COMMAND'] as $command) {
				$newMessage = CRest::call('imbot.message.add', array(
					'BOT_ID'    => $_REQUEST['data']['PARAMS']['BOT_ID'],
					'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
					'MESSAGE'   => "Это команда [B]".$command['COMMAND']."[/B], но я не пока не знаю как на нее реагировать. Сорян!"
				));
			}
			writeToLog($newMessage, $logs, 'Реакция бота на команду: '.$command);

			break;
		

		default:
			### Не запланированое действие пользователя ###
			$newMessge = CRest::call('imbot.message.add', array(
				'BOT_ID'    => $_REQUEST['data']['PARAMS']['BOT_ID'],
				'DIALOG_ID' => $_REQUEST['data']['PARAMS']['DIALOG_ID'],
				'MESSAGE'   => 'Не делайте это!'
			));
			writeToLog($newMessage, $logs, 'Ответ бота на неизвестное событие');

			break;
	}
}

########################################## functions ######################################################
// writeToLog ($_REQUEST['data'], $logs, 'Новый реквест')
function writeToLog ($data, $bool, $title = 'DEBUG', $file = 'debug.txt') {
	if ($bool) {
		$log = "\n--------------------\n";
		$log .= date('d.m.Y H:i:s')."\n";
		$log .= $title."\n";
		$log .= print_r($data, 1);
		$log .= "\n--------------------\n";
		file_put_contents(__DIR__.'/'.$file, $log, FILE_APPEND);
	}
	return true;
}