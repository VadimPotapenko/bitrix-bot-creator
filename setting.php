<?php
### НАСТРОЙКИ БОТА ###
### основные параметры ###
$handlerUrl = (isset($_SERVER['HTTPS']) ? 'https:' : 'http:').'//'.$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME']; // url скрипта
$botParams = array(
	'CODE'       => 'B_5.0',
	'TYPE'       => 'B',
	'OPENLINE'   => 'N',
	'PROPERTIES' => array(
		'NAME'              => 'Подручный',
		'COLOR'             => 'GREEN',
		'EMAIL'             => 'test@local.dev',
		'PERSONAL_BIRTHDAY' => '2019-11-12',
		'WORK_POSITION'     => 'Бот',
		'PERSONAL_WWW'      => 'https://nicedo.ru',
		'PERSONAL_GENDER'   => 'm',
		'PERSONAL_PHOTO'    => base64_encode(file_get_contents(__DIR__.'/avatar.png'))
	),
	'EVENT_MESSAGE_ADD'     => $handlerUrl,
	'EVENT_WELCOME_MESSAGE' => $handlerUrl,
	'EVENT_BOT_DELETE'      => $handlerUrl
);
### настройка команд ###
$botCommands = array(
	'deals' => array(
		'COMMAND' => 'deals',
		'COMMON'  => 'N',
		'HIDDEN'  => 'N',
		'LANG'    => array(
			array(
				'LANGUAGE_ID' => 'ru',
				'TITLE'       => 'Сделки',
				'PARAMS'      => 'Информация о сделках'
			)
		)
	),
	'helpme' => array(
		'COMMAND' => 'helpme',
		'COMMON'  => 'N',
		'HIDDEN'  => 'N',
		'LANG'    => array(
			array(
				'LANGUAGE_ID' => 'ru',
				'TITLE'       => 'Инструкция',
				'PARAMS'      => 'Подробная инструкция'
			)
		)
	)
);
### настрока событий ###
$events = array('OnAppUpdate', 'ONIMBOTDELETE', 'ONCRMDEALADD', 'ONCRMDEALUPDATE', 'ONCRMDEALDELETE'); // перечень событий
$firstMessage = 'Первое сообщение от бота!';                                         // первое сообщение от бота